# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Intel CPU undervolting and throttling configuration tool"
HOMEPAGE="https://github.com/kitsunyan/intel-undervolt/"
SRC_URI="https://codeload.github.com/kitsunyan/${PN}/tar.gz/${PV} -> ${PN}-${PV}.tar.gz"

# https://github.com/kitsunyan/intel-undervolt/archive/1.6.tar.gz

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND=""
DEPEND="${RDEPEND}"
DOCS=( README.md )

src_install() {
  dosbin intel-undervolt
  dodoc README.md

  insinto /etc
  doins intel-undervolt.conf
}
