# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit desktop gnome2-utils cmake-utils git-r3

DESCRIPTION="Birdtray is a free system tray notification for new mail for Thunderbird"
HOMEPAGE="https://github.com/gyunaev/birdtray"
EGIT_REPO_URI="https://github.com/gyunaev/birdtray.git"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
	dev-db/sqlite
	dev-qt/qtcore:5=
	dev-qt/qtx11extras:5=
	"

src_configure() {
	CMAKE_BUILD_TYPE=Release

	cmake-utils_src_configure
}

src_install() {
	cmake-utils_src_install
}

pkg_preinst() { gnome2_icon_savelist; }
pkg_postinst() { xdg_icon_cache_update; }
pkg_postrm() { xdg_icon_cache_update; }
