# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Arduino command line interface"
HOMEPAGE="https://github.com/arduino/arduino-cli"
SRC_URI="https://github.com/arduino/${PN}/releases/download/${PV}/${PN}_${PV}_Linux_64bit.tar.gz -> ${PN}-${PV}.tar.gz"

RESTRICT="strip"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND=""
DEPEND="${RDEPEND}"
DOCS=(README.md)

S="${WORKDIR}"

src_install() {
  dosbin arduino-cli
  dodoc README.md
}
