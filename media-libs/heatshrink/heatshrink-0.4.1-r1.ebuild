# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="data compression library for embedded/real-time systems"
HOMEPAGE="https://github.com/atomicobject/heatshrink"

inherit cmake

SRC_URI="https://github.com/atomicobject/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64 ~x86"

LICENSE="ISC"
SLOT="0"

PATCHES=(
	"${FILESDIR}/Makefile.patch"
)

src_prepare() {
	cp "${FILESDIR}/Config.cmake.in" "${S}/"
	cp "${FILESDIR}/CMakeLists.txt" "${S}/"

	cmake_src_prepare
}

src_configure() {
	cmake_src_configure
}
