# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake git-r3

DESCRIPTION="Prusa Block & Binary G-code reader / writer / converter"
HOMEPAGE="https://github.com/prusa3d/libbgcode"
EGIT_REPO_URI="https://github.com/prusa3d/${PN}.git"

LICENSE="AGPL-3"
SLOT="0"
KEYWORDS="~amd64"

RESTRICT="test"

RDEPEND="
	<dev-cpp/catch-3
	dev-libs/boost:=[nls]
	dev-python/pybind11
	media-libs/heatshrink
	sys-libs/zlib:=
"

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	cmake_src_configure
}
