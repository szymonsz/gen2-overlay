# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit unpacker

PV_REVISION="1"
DESCRIPTION="Community build of InSpec Auditing and Testing Framework."
HOMEPAGE="http://cinc.sh/"
SRC_URI="http://downloads.cinc.sh/files/stable/${PN}/${PV}/debian/10/cinc-auditor_${PV}-${PV_REVISION}_amd64.deb"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""
RESTRICT="mirror strip"
RDEPEND=""

S=${WORKDIR}

src_install() {
  insinto "/opt"
  doins -r opt/cinc-auditor

  find "${ED}"/opt/cinc-auditor/embedded/bin/ -type f -exec chmod a+rx {} \; || die
  find "${ED}"/opt/cinc-auditor/bin/ -type f -exec chmod a+rx {} \; || die

  dosym "/opt/cinc-auditor/bin/cinc-auditor" "/usr/bin/cinc-auditor"
  dosym "/opt/cinc-auditor/bin/cinc-auditor" "/usr/bin/inspec"
}
