# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit unpacker

PV_REVISION="1"
MY_PN="${PN%%-*}"
DESCRIPTION="A systems integration framework, built to bring the benefits of configuration management to your entire infrastructure."
HOMEPAGE="http://www.chef.io/"
SRC_URI="https://packages.chef.io/files/stable/chef/${PV}/debian/8/chef_${PV}-${PV_REVISION}_amd64.deb"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""
RESTRICT="mirror strip"
RDEPEND="!dev-ruby/chef"

S=${WORKDIR}

src_install() {
  insinto "/opt"
  doins -r opt/chef

  find "${ED}"/opt/chef/embedded/bin/ -type f -exec chmod a+rx {} \; || die
  find "${ED}"/opt/chef/bin/ -type f -exec chmod a+rx {} \; || die

  for binary in chef-client chef-apply chef-solo chef-shell; do
    dosym "/opt/chef/bin/${binary}" "/usr/bin/${binary}"
  done
}
